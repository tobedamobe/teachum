<?php
	
/*
|--------------------------------------------------------------------------
| Profile controller
|--------------------------------------------------------------------------
|
| In this controller profile actions are made.
| 
| This controller contains:
| - USER PROFILE (GET)
| - UPDATE USER DATA (POST)
| - BACKGROUND IMAGE (POST)
| - LANGUAGE (POST)
| - HELPER FUNCTIONS
| - NOT DOCUMENTED
|
*/

class ProfileController extends BaseController{
	
	/*
	|--------------------------------------------------------------------------
	| USER PROFILE
	|--------------------------------------------------------------------------
	|
	| The following functions are for editing the user profile.
	|
	*/

	public function getUserProfile(){
		$user = User::find(Auth::user()->id);
		$backgrounds = $this->getAvaillableBackgroundImages();

		if($user->count()){
			$user = $user->first();
			return View::make('profile.user')
					->with('user',$user)
					->with('backgrounds',$backgrounds);	
		}

		return App::abort(404);
	}

	/*
	|--------------------------------------------------------------------------
	| UPDATE USER DATA
	|--------------------------------------------------------------------------
	|
	| The following functions are for updating the userdata.
	| There is no get-Method because the form for updating user data
	| is part of the user profile.
	|
	*/
	
	public function postUpdateUserdataProfile(){
		$validator = Validator::make(Input::all(), User::$rules_updateuserdata);
		if($validator->passes()){
			$user = User::find(Auth::user()->id);

			$user->firstname 	= 	Input::get('firstname');
    		$user->lastname 	= 	Input::get('lastname');
    		$user->sex 			= 	Input::get('sex');
    		$user->birthday 	= 	Input::get('birthday');

    		if($user->save()){
    			return Redirect::route('profile-get-user')
								->with('success',Lang::get('messages.success-account-update'));
    		}

		}else{
			return Redirect::route('profile-get-user')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
		}

		return Redirect::route('profile-get-user')
    					->with('danger', 'We could not update your profile. Please try again!');
	}

	/*
	|--------------------------------------------------------------------------
	| BACKGROUND IMAGE
	|--------------------------------------------------------------------------
	|
	| The following functions are for setting a background image.
	| There is no get-Method because the form for setting a background image
	| is part of the user profile.
	|
	*/

	public function postUpdateBackgroundProfile(){
		$validator = Validator::make(Input::all(), User::$rules_updatebackground);

		if($validator->passes()){
			$user = User::find(Auth::user()->id);

			$user->background_image = Input::get('background_image');
			
			if($user->save()){
    			return Redirect::route('profile-get-user')
									->with('success','You have successfully changed your background image!');
    		}

		}else{
			return Redirect::route('profile-get-user')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
		}

		return Redirect::route('profile-get-user')
    					->with('danger', 'We could not change your background image. Please try again!');
	}

	/*
	|--------------------------------------------------------------------------
	| LANGUAGE
	|--------------------------------------------------------------------------
	|
	| The following functions are for changing the apllications language.
	| There is no get-Method because the form for setting the language
	| is part of the user profile.
	|
	*/

	public function postUpdateUserLanguageProfile(){
		$validator = Validator::make(Input::all(), User::$rules_updatelanguage);

		if($validator->passes()){
			$user = User::find(Auth::user()->id);

			$user->lang = Input::get('lang');
			
			if($user->save()){
				Session::put('teachum.locale', $user->lang);

    			return Redirect::route('profile-get-user')
									->with('success','You have successfully changed your language!');
    		}

		}else{
			return Redirect::route('profile-get-user')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
		}

		return Redirect::route('profile-get-user')
    					->with('danger', 'We could not change the language. Please try again!');
	}

	/*
	|--------------------------------------------------------------------------
	| HELPER FUNCTIONS
	|--------------------------------------------------------------------------
	|
	| The following functions are private helper functions for the 
	| profile controller.
	|
	*/

	private function getAvaillableBackgroundImages(){
		$files = File::allFiles(public_path().'/img/bg/');
		return $files;
	}

	/*
	|--------------------------------------------------------------------------
	| NOT DOCUMENTED
	|--------------------------------------------------------------------------
	|
	| Needs to be documented!!!
	|
	*/

	public function getUserProfileOnlyTeacher(){
		return "Just for Teachers";
	}

}

?>