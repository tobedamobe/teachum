<?

/*
|--------------------------------------------------------------------------
| Account controller
|--------------------------------------------------------------------------
|
| In this controller account actions are made.
| 
| This controller contains:
| - ACCOUNT CREATION (GET, POST)
| - ACCOUNT DELETION (POST)
| - CHANGE PASSWORD (GET, POST)
| - LOGIN (GET, POST)
| - LOGOUT (GET)
| - ACTIVATE ACCOUNT (GET)
|
*/

class AccountController extends BaseController{
	
	/*
	|--------------------------------------------------------------------------
	| ACCOUNT CREATION
	|--------------------------------------------------------------------------
	|
	| The following functions are for account creation
	|
	*/

	public function getCreateAccount(){
		return View::make('account.create');
	}

	public function postCreateAccount(){
		$validator = Validator::make(Input::all(),User::$rules);
		if ($validator->passes()) {
        	$user = new User;

    		$user->firstname 	=	Input::get('firstname');
    		$user->lastname 	=	Input::get('lastname');
    		$user->sex 			=	Input::get('sex');
    		$user->birthday 	=	Input::get('birthday');
    		$user->email 		=	Input::get('email');
    		$username 			=	Input::get('username');
    		
    		$user->username 	=	$username;

    		
    		$user->password 	= 	Hash::make(Input::get('password'));

    		$code 				= 	str_random(60);
    		$user->code 		= 	$code;
    		$user->active 		= 	0;

    		$user->save();

    		if($user){

    			Mail::send('emails.auth.activate', array('link'=>URL::route('account-get-activate',$code),'username'=>$username),function($message) use ($user) {
    				$message->to($user->email,$user->username)->subject('Activate your Teachum-Account');
    			});

    			return Redirect::route('home-get')->with('info', 'Thanks for registering! We sent you an email for activation. Please check your emails.');
    		}
 
    		
    	} else {
        	return Redirect::route('account-get-create')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
    	}
	}

	public function getSchools(){
		$html = new Htmldom('http://de.wikipedia.org/wiki/Liste_von_Schulen_in_Nordrhein-Westfalen');

		http://packalyst.com/packages/package/yangqi/htmldom     
	}

	/*
	|--------------------------------------------------------------------------
	| ACCOUNT DELETION
	|--------------------------------------------------------------------------
	|
	| The following functions are for account deletion
	| There is no get-Method because the form for deleting a user
	| is part of the user profile.
	|
	*/
	
	public function postDeleteUserAccount(){
		$validator = Validator::make(Input::all(), User::$rules_deleteuseraccount);
		if($validator->passes()){
			$user 		= 	User::find(Auth::user()->id);
			$password 	= 	Input::get('password');
			
			if(Hash::check($password, $user->password)){
				$user->delete();

				return Redirect::route('account-get-signin')
    					->with('success',Lang::get('messages.success-account-delete'));
			}

		}else{
			return Redirect::route('profile-get-user')
        					->with('form-message', Lang::get('messages.error-forms'))
        					->withErrors($validator)
        					->withInput();
		}

		return Redirect::route('profile-get-user')
    					->with('danger', Lang::get('messages.error-account-delete'));
	}
	
	/*
	|--------------------------------------------------------------------------
	| CHANGE PASSWORD
	|--------------------------------------------------------------------------
	|
	| The following functions are for changing password
	|
	*/
	
	public function getChangePasswordAccount(){
		return View::make('account.changepassword');
	}

	public function postChangePasswordAccount(){
		$validator = Validator::make(Input::all(), User::$rules_changepassword);
		if($validator->passes()){
			$user = User::find(Auth::user()->id);
			$old_password	=	Input::get('old_password');
			$new_password	=	Input::get('new_password');

			if(Hash::check($old_password,$user->getAuthPassword())){
				$user->password = Hash::make($new_password);

				if($user->save()){
					return Redirect::route('home-get')
									->with('success','You have successfully changed your password!');
				}
			}else{
				return Redirect::route('account-get-changepassword')
        						->with('form-message', 'Your old password is incorrect.');
			}

		}else{
			return Redirect::route('account-get-changepassword')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
		}

		return Redirect::route('account-get-changepassword')
    					->with('danger', 'We could not change your password. Please try again!');
	}
	
	/*
	|--------------------------------------------------------------------------
	| LOGIN
	|--------------------------------------------------------------------------
	|
	| The following functions are for logging in
	|
	*/
	
	public function getSignInAccount(){
		return View::make('account.signin');
	}

	public function postSignInAccount(){
		$validator = Validator::make(Input::all(),User::$rules_login);
		
		if ($validator->passes()) {
			
			$remember_me = (Input::has('remember')) ? true : false;
			
			$auth = Auth::attempt(array(
				'username' => Input::get('username'),
				'password' => Input::get('password'),
				'active' => 1
			),$remember_me);

			

			if($auth){
				return Redirect::intended('/')
								->with('success', 'Welcome back!!');
			}else{
				return Redirect::route('account-get-signin')
								->with('form-message', 'Username/Password wrong or account not activated!');
			}

		}else {
        	return Redirect::route('account-get-signin')
        					->with('form-message', 'The following errors occurred')
        					->withErrors($validator)
        					->withInput();
    	}

    	return Redirect::route('account-get-signin')
    					->with('form-message', 'There was a problem, please try later!');		
	}

	/*
	|--------------------------------------------------------------------------
	| LOGOUT
	|--------------------------------------------------------------------------
	|
	| The following functions are for logging out.
	| There is no post-Method. Logout just quits the session.
	|
	*/
	
	public function getLogoutAccount(){
		Auth::logout();
		return Redirect::route('account-get-signin')->with('success','You have successfully been logged out!');
	}
	
	/*
	|--------------------------------------------------------------------------
	| ACTIVATE ACCOUNT
	|--------------------------------------------------------------------------
	|
	| The following functions are for activate your account.
	| There is no post-Method because the activation code just have to
	| be received. No sending is neccessary.
	|
	*/
	
	public function getActivateAccount($code){
		$user = User::where('code','=',$code)->where('active','=',0);

		if($user->count()){
			$user = $user->first();
			
			$user->active = 1;
			$user->code = '';

			if($user->save()){
				return Redirect::route('home-get')
								->with('info', 'Your account is now activated!');
			}
		}

    	return Redirect::route('home-get')
    					->with('danger', 'We could not activate your account. Maybe it is already activated or the activation code is wrong. Please try again!');	
	}

	/*
	|--------------------------------------------------------------------------
	| CREATE ROLES AND PERMISSIONS
	|--------------------------------------------------------------------------
	|
	|
	*/

	public function createRolesAndPermissions(){
		$teacher = new Role;
		$teacher->name = 'Teacher';
		$teacher->save();

		$student = new Role;
		$student->name = 'Student';
		$student->save();

		$tobe = User::where('username','=','tobe')->first();
		$tobe->attachRole( $teacher );

		$leme = User::where('username','=','leme')->first();
		$leme->attachRole( $student );

		$create_topics = new Permission;
		$create_topics->name = 'create_topics';
		$create_topics->display_name = 'Create Topics';
		$create_topics->save();

		$edit_topics = new Permission;
		$edit_topics->name = 'edit_topics';
		$edit_topics->display_name = 'Edit Topics';
		$edit_topics->save();

		$delete_topics = new Permission;
		$delete_topics->name = 'delete_topics';
		$delete_topics->display_name = 'Delete Topics';
		$delete_topics->save();

		$read_topics = new Permission;
		$read_topics->name = 'read_topics';
		$read_topics->display_name = 'Read Topics';
		$read_topics->save();

		$assign_students = new Permission;
		$assign_students->name = 'assign_students';
		$assign_students->display_name = 'Assign Students';
		$assign_students->save();

		$enrole = new Permission;
		$enrole->name = 'enrole';
		$enrole->display_name = 'Enrole';
		$enrole->save();

		$teacher->perms()->sync(array(
			$create_topics->id,
			$edit_topics->id,
			$delete_topics->id,
			$read_topics->id,
			$assign_students->id

		));
		$student->perms()->sync(array(
			$read_topics->id,
			$enrole->id
		));
	}
	
}

?>