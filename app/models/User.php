<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {
    use HasRole;
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var    string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var    array
	 */
	protected $hidden = array('password','password_temp','code', 'remember_token');

    /**
     * This function returns the password of the gven user
     *
     * @return  string
     */
	public function getAuthPassword(){
		return $this->password;
	}
    

    /**
     * This function generates an md5-Hash for Gravatar from a user email
     * and returns the url that is neccessary to get a gravatar.
     *
     * @return   string
     */
	public function getGravatarAttribute(){	
    	$hash = md5(strtolower(trim($this->attributes['email'])));
    	return "http://www.gravatar.com/avatar/$hash?s=280&d=mm";
	}


    /*
    |--------------------------------------------------------------------------
    | Validation rules
    |--------------------------------------------------------------------------
    |
    | In this section the validation rules for user specific forms are 
    | implemented.
    |
    */
    

    /**
     * This is the array that contains the validation rules for 
     * the signup form
     *
     * @var   array
     */
	public static $rules = array(
        'firstname'=>'alpha|min:2',
        'lastname'=>'alpha|min:2',
        'birthday' => 'date',
        'sex' => 'required|in:m,f',
        'username'=>'required|alpha|min:4|unique:users',
        'email'=>'required|email|unique:users',
        'password'=>'required|alpha_num|min:6',
        'confirm_password'=>'required|alpha_num|min:6|same:password'
    );

    /**
     * This is the array that contains the validation rules for 
     * the login form
     *
     * @var   array
     */
    public static $rules_login = array(
        'username'=>'required|alpha|min:4',
        'password'=>'required|alpha_num|min:6'
    );

    /**
     * This is the array that contains the validation rules for 
     * the changepassword form
     *
     * @var   array
     */
    public static $rules_changepassword = array(
        'old_password'=>'required|alpha_num|min:6',
        'new_password'=>'required|alpha_num|min:6',
        'confirm_password'=>'required|same:new_password'
    );

    /**
     * This is the array that contains the validation rules for 
     * the update-userdata form
     *
     * @var   array
     */
    public static $rules_updateuserdata = array(
        'firstname'=>'alpha|min:2',
        'lastname'=>'alpha|min:2',
        'birthday' => 'date',
        'sex' => 'required|in:m,f'
    );

    /**
     * This is the array that contains the validation rules for 
     * the delete-user-account form
     *
     * @var   array
     */
    public static $rules_deleteuseraccount = array(
        'password'=>'required|alpha_num'
    );

    /**
     * This is the array that contains the validation rules for 
     * the update-usersettings form
     *
     * @var   array
     */
    public static $rules_updatebackground = array(
        'background_image'=> array('required','regex:/\d\d\.(jpg|png)/')
    );

    /**
     * This is the array that contains the validation rules for 
     * the update-language form
     *
     * @var   array
     */
    public static $rules_updatelanguage = array(
        'lang' => 'required|in:de,en'
    );

    

    

}
