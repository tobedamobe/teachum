<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
	'as'=>'home-get',
	'uses' => 'HomeController@getHome'
));

/* This is the authenticated group */
Route::group(array('before'=>'auth'),function(){

		/*
			TEST: Role based access (teachers only)
		*/

		// Route::group(array('before'=>'role'),function(){

		// 	Route::get('/profile/only-teacher', array(
		// 		'as' => 'profile-get-user-onlyteacher',
		// 		'uses' => 'ProfileController@getUserProfileOnlyTeacher'
		// 	));

		// 	Route::get('/account/getSchools', array(
		// 		'as' => 'account-get-schools',
		// 		'uses' => 'AccountController@getSchools'
		// 	));

			
			
		// });

			Route::get('/account/create-roles-and-permissions', array(
				'as' => 'create-roles-and-permissions',
				'uses' => 'AccountController@createRolesAndPermissions'
			));

		/* This is the anti-csrf group */
		Route::group(array('before'=>'csrf'),function(){
			/*
				Change Password (POST)
			*/
			Route::post('/account/changepassword',array(
				'as' => 'account-post-changepassword',
				'uses' => 'AccountController@postChangePasswordAccount'
			));

			/*
				Update Userdata (POST)
			*/
			Route::post('/profile/updateuserdata',array(
				'as' => 'profile-post-updateuserdata',
				'uses' => 'ProfileController@postUpdateUserdataProfile'
			));

			/*
				Update Profile Settings (POST)
			*/
			Route::post('/profile/updatebackground',array(
				'as' => 'profile-post-updatebackground',
				'uses' => 'ProfileController@postUpdateBackgroundProfile'
			));

			/*
				Update user language (POST)
			*/
			Route::post('/profile/updateuserlanguage',array(
				'as' => 'profile-post-updateuserlanguage',
				'uses' => 'ProfileController@postUpdateUserLanguageProfile'
			));

			

			/*
				Delete User (DELETE)
			*/
			Route::delete('/account/deleteuser',array(
				'as' => 'account-post-deleteuser',
				'uses' => 'AccountController@postDeleteUserAccount'
			));

			

		});

		/*
			Logout Account (GET)
		*/
		Route::get('/account/logout',array(
			'as' => 'account-get-logout',
			'uses' => 'AccountController@getLogoutAccount'
		));

		/*
			Logout Account (GET)
		*/
		Route::get('/profile',array(
			'as' => 'profile-get-user',
			'uses' => 'ProfileController@getUserProfile'
		));

		/*
			Change Password (GET)
		*/
		Route::get('/account/changepassword',array(
			'as' => 'account-get-changepassword',
			'uses' => 'AccountController@getChangePasswordAccount'
		));
});





/* This is the unauthenticated group */
Route::group(array('before'=>'guest'),function(){

	/* This is the anti-csrf group */
	Route::group(array('before'=>'csrf'),function(){
		
		/*
			Create Account (PUT)
		*/
		Route::put('/account/create',array(
			'as' => 'account-post-create',
			'uses' => 'AccountController@postCreateAccount'
		));

		/*
			Sign-In (POST)
		*/
		Route::post('/account/signin',array(
			'as' => 'account-post-signin',
			'uses' => 'AccountController@postSignInAccount'
		));

	});

	/*
			Create Account (GET)
	*/
	Route::get('/account/create',array(
		'as' => 'account-get-create',
		'uses' => 'AccountController@getCreateAccount'
	));

	/*
			Sign-In (GET)
	*/
	Route::get('/account/signin',array(
		'as' => 'account-get-signin',
		'uses' => 'AccountController@getSignInAccount'
	));

	/*
			Activate Account (GET)
	*/
	Route::get('/account/activate/{code}',array(
		'as' => 'account-get-activate',
		'uses' => 'AccountController@getActivateAccount'
	));

});


