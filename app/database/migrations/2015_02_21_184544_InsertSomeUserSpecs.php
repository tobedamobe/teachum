<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertSomeUserSpecs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
    		$table->string('password_temp', 64)->after('password');
    		$table->integer('active')->unsigned()->after('remember_token');
    		$table->string('code', 60)->after('remember_token');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table)
		{
    		$table->dropColumn(array('password_temp', 'active', 'code'));
		});
	}

}
