<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SeedUsersTableTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->truncate();

			$users = [
            [
            	'username' => 'kaja',
                'firstname' => 'Katja',
                'lastname' => 'Ellberg',
                'email' => 'k_ellberg@yahoo.de',
                'password' => Hash::make('75127512')
            ],
            [
            	'username' => 'toby',
                'firstname' => 'Tobias',
                'lastname' => 'Grieß',
                'email' => 'tobe_damobe@me.com',
                'password' => Hash::make('W4lter1554cson')
            ],
        ];

        foreach($users as $user){
            User::create($user);
        }
	}

}