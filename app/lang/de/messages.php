<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'error-forms' 				=> 	'Folgende Fehler sind aufgetreten',
    'success-account-delete' 	=> 	'Dein Profil wurde erfolgreich gelöscht! Einen schönen Tag noch.',
    'error-account-delete' 		=> 	'Wir konnten dein Profil nicht löschen. Vielleicht ist das Passwort falsch. Versuch es nochmal!',
    'success-account-update' 	=> 	'Dein Profil wurde erfolgreich aktualisiert!',



 	/*
	|--------------------------------------------------------------------------
	| Description texts
	|--------------------------------------------------------------------------
	|
	| The following entries are the description texts that are used to explain forms
	|
	*/

    'userdata-description'		=>  'Hier kannst du dir deine persönlichen Daten ansehen oder sie verändern. Sie sind nicht zwingend notwendig aber helfen dir dein Profil benutzerfreundlicher und personalisierter zu machen.',
    'background-description'	=>	'Wähle dein Hintergrundbild. Vergiss nicht zu speichern, bevor du diese Seite verlässt!',
    'security-description'		=>	'Hier kannst du deine Sicherheitseinstellungen ansehen oder verändern. Wenn dir etwas komisch, falsch oder nicht von dir authorisiert vorkommt, ändere bitte sofort dein Passwort und kontaktiere uns.',
    'delete-description'		=>	'Wenn du Teachum nicht mehr benutzen möchtest, gib hier dein Passwort ein, klick den Button und habe einen schönen Tag! :)',
	'delete-description-2'		=>	'Bitte sei dir im klaren darüber, dass dein Account unwiderruflich gelöscht wird. Dies bezieht sich auf alle Kurse und anderes Zeug!',
	'language-description'		=>	'Hier kannst du die Sprache von Teachum ändern. Wähle deine Sprache und klicke dann auf Speichern.',

	 /*
	|--------------------------------------------------------------------------
	| Titles and headers
	|--------------------------------------------------------------------------
	|
	| The following entries are the box-headers and titles of sections
	|
	*/

	'userdata-title'					=>	'Benutzerdaten',
	'security-title'					=>	'Sicherheit',
	'language-title'					=>	'Sprache',
	'background-title'					=>	'Hintergrund',
	'delete-title'						=>	'Account löschen',
	'changepassword-title'				=>	'Passwort ändern',
	'signin-title'						=>	'Anmelden',
	'signup-title'						=>	'Registrieren',
	'logout-title'						=>	'Abmelden',
	'profile-title'						=>	'Profil',


    /*
	|--------------------------------------------------------------------------
	| Frequently used words
	|--------------------------------------------------------------------------
	|
	| The following entries are the words thats are often used like Username, Email, etc.
	|
	*/

	'warning'					=>	'Warnung',
	'important'					=>	'Wichtig',
	'teacher'					=>	'Lehrer',
	'student'					=>	'Schüler',
	'accept'					=>  'Akzeptieren',
	'cancel'					=>	'Abbrechen',
	'save'						=>  'Speichern',
	'reset'						=>	'Rückgängig machen',
	'delete'					=>	'Löschen',
	'german'					=>	'German',
	'english'					=>	'English',
	'male'						=>	'männlich',
	'female'					=>	'weiblich',
	'role'						=>	'Rolle',
	'perms'						=>	'Rechte',
	'member-since'				=>	'Mitglied seit',
	'last-updated'				=>	'Letztes Update am',
	'firstname'					=>	'Vorname',
	'lastname'					=>	'Nachname',
	'username'					=>	'Benutzername',
	'birthday'					=>	'Geburtstag',
	'password'					=>	'Passwort',
	'old-password'				=>	'Altes Passwort',
	'new-password'				=>	'Neues Passwort',
	'confirm-new-password'		=>	'Neues Passwort bestätigen',
	'confirm-password'			=>	'Passwort bestätigen',
	'rememberme'				=>	'Erinnere dich an mich',
];

?>