<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'error-forms' 				=> 	'The following errors occurred',
    'success-account-delete' 	=> 	'You have successfully deleted your profile! Have a nice day.',
    'error-account-delete' 		=> 	'We could not delete your profile. Maybe the password is wrong. Please try again!',
    'success-account-update' 	=> 	'Your profile has successfully been updated!',



    /*
	|--------------------------------------------------------------------------
	| Description texts
	|--------------------------------------------------------------------------
	|
	| The following entries are the description texts that are used to explain forms
	|
	*/

    'userdata-description'		=> 	'Here you can view or change your personal data. They are not required but they help you to create a more user friendly and personalized profile.',
    'background-description'	=>	'Select your favourite background image. Don`t forget to save your settings before you leave this page!',
    'security-description'		=>	'Here you can view or change your security settings. If something seems weird, wrong or even not authenticated, please change your password and contact us.',
    'delete-description'		=>	'If you don´t want to use Teachum anymore, type in your password, hit the button and have a good day! :)',
	'delete-description-2'		=>	'Please realize that the deletion of your account is irrecoverable. It will affect all of your courses and other stuff!',
    'language-description'		=>	'Here you can change the language of Teachum. Please choose your language and hit Save',

	 /*
	|--------------------------------------------------------------------------
	| Titles and headers
	|--------------------------------------------------------------------------
	|
	| The following entries are the box-headers and titles of sections
	|
	*/

	'userdata-title'					=>	'User data',
	'security-title'					=>	'Security',
	'language-title'					=>	'Language',
	'background-title'					=>	'Background',
	'delete-title'						=>	'Delete account',
	'changepassword-title'				=>	'Change password',
	'signin-title'						=>	'Login',
	'signup-title'						=>	'Signup',
	'logout-title'						=>	'Logout',
	'profile-title'						=>	'Profile',


    /*
	|--------------------------------------------------------------------------
	| Frequently used words
	|--------------------------------------------------------------------------
	|
	| The following entries are the words thats are often used like Username, Email, etc.
	|
	*/

	'warning'					=>	'Warning',
	'important'					=>	'Important',
	'teacher'					=>	'Teacher',
	'student'					=>	'Student',
	'accept'					=>  'Accept',
	'cancel'					=>	'Cancel',
	'save'						=>  'Save',
	'reset'						=>	'Reset',
	'delete'					=>	'Delete',
	'german'					=>	'Deutsch',
	'english'					=>	'Englisch',
	'male'						=>	'male',
	'female'					=>	'female',
	'role'						=>	'Role',
	'perms'						=>	'Permissions',
	'member-since'				=>	'Member since',
	'last-updated'				=>	'Last update',
	'firstname'					=>	'Firstname',
	'lastname'					=>	'Lastname',
	'username'					=>	'Username',
	'birthday'					=>	'Birthday',
	'password'					=>	'Password',
	'old-password'				=>	'Old password',
	'new-password'				=>	'New password',
	'confirm-new-password'		=>	'New password again',
	'confirm-password'			=>	'Password again',
	'rememberme'				=>	'Remember me',
];

?>