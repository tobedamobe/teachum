{{-- #################################################
			
TODO: 
- Validation-Hints better show inside the inputs! http://getbootstrap.com/css/#forms-control-validation

################################################# --}}

@extends('layout.main')

@section('content')

@section('title')
	 • Login
@stop

<div class="row">
	<div class="col-lg-4 col-lg-offset-4">
		<div class="white-container top-lined">

			{{-- ################################################# 
			
			FORM-ERROR MESSAGE AREA 

			################################################# --}}
			
			@if(Session::has('form-message'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>{{ Lang::get('messages.warning') }}!</strong> {{ Session::get('form-message') }}
				</div>
			@endif
			
			<ul class="list-group">
				@foreach($errors->all() as $error)
					<li class="list-group-item"><small>{{ $error }}</small></li>
				@endforeach
			</ul>


			{{-- ################################################# 

			LOGIN AREA 

			#################################################  --}}

			<h2>{{ Lang::get('messages.signin-title') }}</h2>

			{{ Form::open(array('route' => 'account-post-signin', 'method' => 'post')) }} 
				<div class="form-group">
					{{ Form::text('username',(Input::old('username')) ? Input::old("username") : '',array('class'=>'form-control','placeholder'=>Lang::get('messages.username'))) }}
				</div> 

				<div class="form-group">
					{{ Form::password('password',array('class'=>'form-control','placeholder'=>Lang::get('messages.password'))) }}
				</div> 

				<div class="checkbox">
   					<label>
    					{{ Form::checkbox('remember') }} {{ Lang::get('messages.rememberme') }}!
    				</label>
  				</div>

				<div class="text-right">
					{{ HTML::linkRoute('home-get', Lang::get('messages.cancel'), array(), array('class' => 'btn btn-link')) }}
					{{ Form::submit(Lang::get('messages.signin-title'),array('class' => 'btn btn-success')) }}
				</div>

			{{ Form::close() }}

		</div> {{-- .white-container --}}
	</div> {{-- .col-*-4 --}}
</div> {{-- .row --}}
@stop