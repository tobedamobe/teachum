{{-- ################################################# 
			
TODO: 
- Validation-Hints better show inside the inputs! http://getbootstrap.com/css/#forms-control-validation

################################################# --}}

@extends('layout.main')

@section('title')
	 • Create Account 
@stop

@section('content')
<div class="row">
	<div class="col-lg-4 col-lg-offset-4">
		<div class="white-container">
			
			{{-- ################################################# 
			
			FORM-ERROR MESSAGE AREA 

			################################################# --}}
			
			@if(Session::has('form-message'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>{{ Lang::get('messages.warning') }}!</strong> {{ Session::get('form-message') }}
				</div>
			@endif

			<ul class="list-group">
				@foreach($errors->all() as $error)
				<li class="list-group-item"><small>{{ $error }}</small></li>
				@endforeach
			</ul>

			{{-- ################################################# 

			SIGN-UP AREA 

			#################################################  --}}

			<h2>{{ Lang::get('messages.signup-title') }}</h2>

			{{ Form::open(array('route' => 'account-post-create', 'method' => 'put')) }}
				<div class="form-group">
					{{ Form::text('firstname',null,array('class'=>'form-control','placeholder'=>Lang::get('messages.firstname'))) }}
				</div> 
				
				<div class="form-group">
					{{ Form::text('lastname',null,array('class'=>'form-control','placeholder'=>Lang::get('messages.lastname'))) }}
				</div> 

				<div class="form-group">
					{{ Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email')) }}
				</div> 
		
				<div class="form-group">
					{{ Form::text('username',null,array('class'=>'form-control','placeholder'=>Lang::get('messages.username'))) }}
				</div> 

				<div class="form-group">
					{{ Form::radio('sex', 'm', true) }} {{ Lang::get('messages.male') }}
					{{ Form::radio('sex', 'f', false) }} {{ Lang::get('messages.female') }}
				</div>

				<div class="form-group">	
					<div class="input-group date">
    	        		{{ Form::text('birthday',null,array('id'=>'birthday-picker-updateuserdata','class'=>'form-control','placeholder'=>Lang::get('messages.birthday'))) }}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
    	   		 	</div>
				</div>

				<div class="form-group">
					{{ Form::password('password',array('class'=>'form-control','placeholder'=>Lang::get('messages.password'))) }}
				</div> 

				<div class="form-group">
					{{ Form::password('confirm_password',array('class'=>'form-control','placeholder'=>Lang::get('messages.confirm-password'))) }}
				</div> 

				<div class="text-right">
					{{ HTML::linkRoute('home-get', Lang::get('messages.cancel'), array(), array('class' => 'btn btn-link')) }}
					{{ Form::submit(Lang::get('messages.accept'),array('class' => 'btn btn-success')) }}
				</div>

			{{ Form::close() }}

		</div> {{-- .white-container --}}
	</div> {{-- .col-*-4 --}}
</div> {{-- .row --}}
@stop