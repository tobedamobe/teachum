{{-- #################################################
			
TODO: 
- Validation-Hints better show inside the inputs! http://getbootstrap.com/css/#forms-control-validation

################################################# --}}

@extends('layout.main')

@section('title')
	 • Change Password 
@stop

@section('content')
<div class="row">
	<div class="col-lg-4 col-lg-offset-4">
		<div class="white-container">
			

			{{-- ################################################# 
			
			FORM-ERROR MESSAGE AREA 

			################################################# --}}

			@if(Session::has('form-message'))
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>{{ Lang::get('messages.warning') }}!</strong> {{ Session::get('form-message') }}
				</div>
			@endif

			<ul class="list-group">
				@foreach($errors->all() as $error)
					<li class="list-group-item">
						<small>{{ $error }}</small>
					</li>
				@endforeach
			</ul>


			{{-- ################################################# 

			CHANGE PASSWORD AREA 

			#################################################  --}}
		
			<h2>{{ Lang::get('messages.changepassword-title') }}</h2>

			{{ Form::open(array('route' => 'account-post-changepassword', 'method' => 'post')) }}

				<div class="form-group">
					{{ Form::password('old_password',array('class'=>'form-control','placeholder'=>Lang::get('messages.old-password'))) }}
				</div> 

				<div class="form-group">
					{{ Form::password('new_password',array('class'=>'form-control','placeholder'=>Lang::get('messages.new-password'))) }}
				</div> 

				<div class="form-group">
					{{ Form::password('confirm_password',array('class'=>'form-control','placeholder'=>Lang::get('messages.confirm-new-password'))) }}
				</div> 

				<div class="text-right">
					{{ link_to(URL::previous(), Lang::get('messages.cancel'), array('class' => 'btn btn-link')) }}
					{{ Form::submit(Lang::get('messages.accept'),array('class' => 'btn btn-success')) }}
				</div>

			{{ Form::close() }}

		</div> {{-- .white-container --}}
	</div>	{{-- .col-*-4 --}}
</div>	{{-- .row --}}
@stop