{{-- ################################################# 
      
TODO:
- Show inline login-form

################################################# --}}

<nav class="navbar navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="glyphicon glyphicon-option-vertical"></span>
        
      </button>

      {{-- Brand, Logo, Icon, Name --}}
      <a class="navbar-brand" href="{{ URL::route('home-get') }}"><img class="logo" src="{{ URL::to('/') }}/img/icons/Muetze_rgb.png" />&nbsp;Teachum</a>
    </div>

    <div id="navbar" class="navbar-collapse collapse">
      
      {{-- ################################################# 
      
      NAVIGATION AREA 

      ################################################# --}}

      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ URL::route('home-get') }}">Home</a></li>
        
        {{-- Auth specific menus: LOGGED IN --}}
        @if(Auth::check())

          {{-- Profile dropdown --}}
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->username }} <span class="glyphicon glyphicon-user"></span></a>
            <ul class="dropdown-menu" role="menu">   
              <li><a href="{{ URL::route('profile-get-user') }}">{{ Lang::get('messages.profile-title') }}</a></li>
              <li><a href="{{ URL::route('account-get-changepassword') }}">{{ Lang::get('messages.changepassword-title') }}</a></li>
              <li class="divider"></li>
              <li><a href="{{ URL::route('account-get-logout') }}">{{ Lang::get('messages.logout-title') }}</a></li>
            </ul>
          </li>

        {{-- Auth specific menus: LOGGED OUT --}}
        @else
          <li><a href="{{ URL::route('account-get-signin') }}">{{ Lang::get('messages.signin-title') }}</a></li>
          <li><a href="{{ URL::route('account-get-create') }}">{{ Lang::get('messages.signup-title') }}</a></li>
        @endif

        

      </ul> {{-- .nav .navbar-nav .navbar-right --}}
    </div> {{-- .navbar-collapse .collapse --}}
  </div> {{-- .container --}}
</nav> {{-- .navbar .navbar-fixed-top --}}


{{-- ################################################# 
      
MESSAGES AREA 

################################################# --}}

<div class="container">
  @if(Session::has('info'))
    <div class="alert alert-info">{{ Session::get('info') }}
      <a href="#" class="close" data-dismiss="alert">&times;</a>
    </div>
  @endif

  @if(Session::has('danger'))
    <div class="alert alert-danger">{{ Session::get('danger') }}
      <a href="#" class="close" data-dismiss="alert">&times;</a>
    </div>
  @endif

  @if(Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}
      <a href="#" class="close" data-dismiss="alert">&times;</a>
    </div>
  @endif
</div> {{-- .container --}}



