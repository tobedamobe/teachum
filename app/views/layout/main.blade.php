{{-- ################################################# 
      
TODO:
- Bootstrap Growl notifications http://bootstrap-growl.remabledesigns.com

################################################# --}}

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    {{-- ################################################# 
      
    TITLE AREA: Here the blade section with the name 'title' will be loaded

    ################################################# --}}  

    <title>
      Teachum @yield('title')
    </title>

    {{-- Latest compiled and minified Bootstrap-CSS --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    {{-- Latest compiled and minified FontAwesome-CSS --}}
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    {{-- Latest compiled and minified GoogleFont-CSS --}}
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic|Oxygen' rel='stylesheet' type='text/css'>
    {{-- Latest compiled and minified Datepicker-CSS --}}
    <link href="http://eternicode.github.io/bootstrap-datepicker/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet">
	 
    {{-- Load the own style.css --}}
    {{ HTML::style('css/style.css'); }}
    
    {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
    {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>


  {{-- ################################################# 
      
  BACKGROUND PICTURE

  This condition loads the opening body-tag with the users 
  background-image if he/she is logged in.
  
  If not, the default image will wie loaded.

  ################################################# --}}

  @if(Auth::check())
    <body style="background-image: url('{{ URL::to('/') }}/img/bg/{{ Auth::user()->background_image }}');">
  @else
    <body style="background-image: url('{{ URL::to('/') }}/img/bg/00.jpg');">
  @endif


    {{-- ################################################# 
      
    NAVIGATION AREA: Here the Navigation Blade-Template will be included

    ################################################# --}}

    @include('layout.navigation')
  	

    {{-- ################################################# 
      
    CONTENT AREA: Here the blade section with the name 'content' will be loaded

    ################################################# --}}  
    <div class="container">
       @yield('content')
    </div>


    
    {{-- ################################################# 
      
    SCRIPTS AREA: Here the used scripts and Plugins will be loaded

    ################################################# --}}  

    {{-- jQuery (necessary for Bootstrap's JavaScript plugins) --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    
    {{-- Latest compiled and minified Bootstrap-JS --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    
    {{-- Latest compiled and minified Datepicker-JS --}}
    <script src="http://eternicode.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    
  
    {{-- www.eyecon.ro/bootstrap-datepicker/ --}}
    <script>
      $('.input-group.date').datepicker({
        format: "yyyy-mm-dd",
        startDate: "1900-01-01",
        endDate: "2015-01-01",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
      });

      $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
      }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
      });

  </script>

  </body>
</html>